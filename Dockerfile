FROM python:3.11-slim
RUN pip install flask
RUN pip install redis
WORKDIR /app
COPY app/flask_app.py .
EXPOSE 8080
CMD flask --app flask_app run -p 8080 -h 0.0.0.0 --debugger