"""This module does blah blah."""

import sys
import random

def binary_search(arr, target):
    """
    Функйи бинарного поиска
    """
    low = 0
    mid = len(arr) // 2
    high = len(arr) - 1
    while low + 1 < high:
        if target < arr[mid]:
            high = mid
            mid //= 2
        elif target > arr[mid]:
            low = mid
            mid = low + (high - low) // 2
        else:
            return mid
    return "Not found"


if __name__ == '__main__':
    arg = int(sys.argv[1])
    data = [random.randint(1, 100) for _ in range(100)]
    data.sort()

    print(data)
    print(*binary_search(arr=data, target=arg))
