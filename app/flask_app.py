"""This module does blah blah."""

from flask import Flask, jsonify
from flask import request
import redis
import random

def binary_search(arr, target):
    """
    Функйи бинарного поиска
    """
    low = 0
    mid = len(arr) // 2
    high = len(arr) - 1
    while low + 1 < high:
        if target < arr[mid]:
            high = mid
            mid //= 2
        elif target > arr[mid]:
            low = mid
            mid = low + (high - low) // 2
        else:
            return mid
    return -1

def store_array_redis(r, n, *a):
    r.lpush(n, *a)
    
def get_array_redis(r, n):
    string_array = r.lrange(n, -1, 0)
    if string_array == None:
        return []
    return [int(element) for element in string_array]

app = Flask(__name__)

r = redis.Redis(host='redis', port=6379, decode_responses=True)
data = get_array_redis(r, 'array')

if data == []:
    data = [random.randint(1, 100) for _ in range(100)]
    data.sort()
    store_array_redis(r, 'array', *data)

@app.route("/", methods = ['GET'])
def get_app():
    return "<h1>Search App</h1>"
    
@app.route("/array", methods = ['GET'])
def get_array():
    return jsonify(array=data)

@app.route("/search", methods = ['GET'])
def get_search():
    default = jsonify(index=-1)
    search_num = request.args.get('num', type=int)
    
    if search_num == None:
        return default

    ind = binary_search(data, search_num)

    return jsonify(index=ind)
